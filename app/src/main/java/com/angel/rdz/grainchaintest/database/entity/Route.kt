package com.angel.rdz.grainchaintest.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng

@Entity
data class Route (@PrimaryKey(autoGenerate = true) val id: Int? = null,
                  @ColumnInfo(name = "title") val title: String,
                  @ColumnInfo(name = "time") val time: String,
                  @ColumnInfo(name ="points") val points: List<LatLng>)