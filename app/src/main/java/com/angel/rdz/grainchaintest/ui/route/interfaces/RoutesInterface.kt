package com.angel.rdz.grainchaintest.ui.route.interfaces

import com.angel.rdz.grainchaintest.database.entity.Route

interface RoutesInterface {
    fun showDetail(route: Route)
}