package com.angel.rdz.grainchaintest.ui.routeDetail

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.angel.rdz.grainchaintest.R
import com.angel.rdz.grainchaintest.database.AppDatabase
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import kotlinx.android.synthetic.main.activity_route_detail.*
import java.util.concurrent.TimeUnit


class RouteDetailActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var vm: RouteDetailViewModel
    private lateinit var mMap: GoogleMap
    lateinit var polylineOptions : PolylineOptions
    var currentId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_route_detail)
        vm = ViewModelProvider(this).get(RouteDetailViewModel::class.java)
        currentId = intent.getIntExtra("routeId", 0)
        AppDatabase.getAppDataBase(applicationContext)?.let {
            vm.setDataBase(it)
        }
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        delete_btn.setOnClickListener {
            deleteRoute()
        }

        share_btn.setOnClickListener{
            openShareIntent()
        }

    }

    override fun onBackPressed() {
        finish()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        vm.getRouteById(currentId)

        vm.currentRoute.observe(this, Observer {
            mMap = googleMap
            polylineOptions = PolylineOptions()
            polylineOptions.color(Color.RED)
            polylineOptions.width(50f)

            vm.currentRoute.value?.points?.let {
                polylineOptions.addAll(it)
                mMap.addPolyline(polylineOptions)
                mMap.addMarker(MarkerOptions().position(it.first()).title("You start here"))
                mMap.addMarker(MarkerOptions().position(it.last()).title("You End here"))
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(it.first(), 15.2f))
            }
            val millis = vm.currentRoute.value?.time?.toLong()
            millis?.let {
                time_tv.text = String.format("%02d min, %02d sec",
                    TimeUnit.MILLISECONDS.toMinutes(millis),
                    TimeUnit.MILLISECONDS.toSeconds(millis) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
                )
            }
            distance_tv.text = String.format("%.2f km", vm.getDistanceTraveled()/1000)
        })
    }

    private fun deleteRoute() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("ELIMINAR")
        builder.setMessage("Estás seguro que quiere eliminar ésta ruta?")
        builder.setPositiveButton("Sí"){_, _ ->
            vm.deleteRoute()
            finish()
        }
        builder.setNegativeButton("No"){dialog,_ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog? = builder.create()
        dialog?.show()
    }

    private fun openShareIntent() {
        val urlAddress = "https://www.google.com/maps/dir/" +
                "?api=1&origin="+vm.currentRoute.value?.points?.first()?.latitude.toString()+","+vm.currentRoute.value?.points?.first()?.longitude.toString()+"&" +
                "destination="+vm.currentRoute.value?.points?.last()?.latitude.toString()+","+vm.currentRoute.value?.points?.last()?.longitude.toString()+"&" +
                "travelmode=walking"
        var waypoints = ""
        vm.currentRoute.value?.points?.forEach {
            waypoints = waypoints + (if (waypoints == "") "" else "%7C") + it.latitude + "," + it.longitude
        }
        waypoints = "&waypoints=$waypoints"
        val url = "$urlAddress&$waypoints"
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, url)
        startActivity(Intent.createChooser(shareIntent,"Enviar a..."))
    }
}
