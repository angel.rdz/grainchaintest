package com.angel.rdz.grainchaintest.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.angel.rdz.grainchaintest.database.entity.Route

@Dao
interface RouteDao {

    @Query("SELECT * FROM route")
    fun getAll(): List<Route>

    @Query("SELECT * FROM route WHERE id = :routeId")
    fun getRouteById(routeId: Int): Route

    @Insert
    fun insertAll(vararg route: Route)

    @Delete
    fun delete(route: Route)

}