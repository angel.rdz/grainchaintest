package com.angel.rdz.grainchaintest.ui.route.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.angel.rdz.grainchaintest.R
import com.angel.rdz.grainchaintest.database.entity.Route
import com.angel.rdz.grainchaintest.ui.route.interfaces.RoutesInterface
import kotlinx.android.synthetic.main.route_list_item.view.*

class RouteListAdapter(var routeList: ArrayList<Route>) : RecyclerView.Adapter<RouteListAdapter.MyViewHolder>() {

    lateinit var listener: RoutesInterface

    class MyViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var title = v.route_title

        fun bind(route: Route) {
            title.text = route.title
        }
    }

    fun setInterListener(routesInterface: RoutesInterface){
        listener = routesInterface
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.route_list_item, parent, false)
        return MyViewHolder(
            v
        )
    }

    override fun getItemCount(): Int {
        return routeList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(routeList[position])
        holder.itemView.setOnClickListener {
            listener.showDetail(routeList[position])
        }
    }

    fun updateList(routeList: ArrayList<Route>){
        this.routeList = routeList
    }
}