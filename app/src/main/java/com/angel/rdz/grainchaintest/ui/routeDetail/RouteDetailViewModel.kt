package com.angel.rdz.grainchaintest.ui.routeDetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.angel.rdz.grainchaintest.database.AppDatabase
import com.angel.rdz.grainchaintest.database.entity.Route
import com.google.maps.android.SphericalUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RouteDetailViewModel : ViewModel() {
    var currentRoute: MutableLiveData<Route> = MutableLiveData()

    lateinit var db : AppDatabase

    fun setDataBase(myDb: AppDatabase){
        db = myDb
    }

    fun getDistanceTraveled() : Double  {
        return SphericalUtil.computeLength(currentRoute.value?.points)
    }

    fun getRouteById(currentId: Int){
        GlobalScope.launch {
            currentRoute.postValue(db.routesDao().getRouteById(currentId))
        }
    }

    fun deleteRoute(){
        GlobalScope.launch {
            currentRoute.value?.let { db.routesDao().delete(it) }
        }
    }

}