package com.angel.rdz.grainchaintest.database

import android.content.Context
import androidx.room.*
import com.angel.rdz.grainchaintest.database.dao.RouteDao
import com.angel.rdz.grainchaintest.database.entity.Route

@Database(entities = [Route::class], version = 1)
@TypeConverters(LatLngTypeConverter::class)
abstract class AppDatabase : RoomDatabase(){
    abstract fun routesDao(): RouteDao

    companion object {
        var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null){
                synchronized(AppDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "myDB").build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }

}