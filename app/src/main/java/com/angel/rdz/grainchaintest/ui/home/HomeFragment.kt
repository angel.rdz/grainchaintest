package com.angel.rdz.grainchaintest.ui.home

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.angel.rdz.grainchaintest.database.AppDatabase
import com.angel.rdz.grainchaintest.utils.GPSTracker
import com.angel.rdz.grainchaintest.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment(), OnMapReadyCallback {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var mMap: GoogleMap
    lateinit var gpstracker : GPSTracker
    lateinit var mainHandler : Handler
    lateinit var runnable : Runnable
    lateinit var fabRecord: FloatingActionButton
    lateinit var polylineOptions : PolylineOptions
    var miliseconds = 0L
    var isRecording = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) { // TODO: Consider calling
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                Companion.LOCATION_REQUEST_CODE
            )
        }

        AppDatabase.getAppDataBase(requireContext())?.let {
            homeViewModel.setDataBase(it)
        }
        runnable = object : Runnable {
            override fun run() {
                homeViewModel.updateLocation(gpstracker.getLatitude(), gpstracker.getLongitude())
                mainHandler.postDelayed(this, 3000)
            }
        }
        fabRecord = root.findViewById(R.id.fab_record)

        return root
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onMapReady(mMap)
                }
                return
            }
            else -> { }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fabRecord.setOnClickListener {
            if (!isRecording) {
                startRecording()
            } else {
                stopRecording()
            }
        }

        homeViewModel.liveDataRouteList.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                drawPolyLineOnMap(it)
                mMap.moveCamera(CameraUpdateFactory.newLatLng(it.last()))
            }
        })
    }

    private fun startRecording() {
        isRecording = true
        chronometer.base = SystemClock.elapsedRealtime()
        chronometer.start()
        fabRecord.setImageDrawable(resources.getDrawable(android.R.drawable.ic_media_pause))
        mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post(runnable)
    }

    private fun stopRecording() {
        isRecording = false
        chronometer.stop()
        miliseconds = SystemClock.elapsedRealtime() - chronometer.base
        showDialog()
        fabRecord.setImageDrawable(resources.getDrawable(android.R.drawable.ic_media_play))
        mainHandler.removeCallbacks(runnable)
    }

    private fun drawPolyLineOnMap(list: List<LatLng>){
        polylineOptions = PolylineOptions()
        polylineOptions.color(Color.RED)
        polylineOptions.width(50f)
        polylineOptions.addAll(list)
        mMap.addPolyline(polylineOptions)
    }

    private fun showDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Route")
        val input = EditText(context)
        input.inputType = InputType.TYPE_CLASS_TEXT
        builder.setView(input)
        builder.setPositiveButton("Guardar") { _, _ ->
            homeViewModel.saveRoute(input.text.toString(), polylineOptions.points, miliseconds.toString())
            clearMap()
        }
        builder.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }

        builder.show()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        gpstracker =
            GPSTracker(requireContext())
        mMap = googleMap
        polylineOptions = PolylineOptions()

        var currentPosition = LatLng(0.0,0.0)
        gpstracker.currentLocation?.let {
            currentPosition = LatLng(it.latitude, it.longitude)
        }

        mMap.addMarker(MarkerOptions().position(currentPosition).title("You are here"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPosition, 18.2f))

    }

    private fun clearMap(){
        mMap.clear()
        homeViewModel.list.clear()
        homeViewModel.liveDataRouteList.value?.clear()
        chronometer.base = SystemClock.elapsedRealtime()
    }

    companion object {
        const val LOCATION_REQUEST_CODE = 1234
    }

}
