package com.angel.rdz.grainchaintest.utils

import android.Manifest
import android.app.AlertDialog
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.IBinder
import android.os.Looper
import android.provider.Settings
import android.util.Log
import androidx.core.app.ActivityCompat
import com.angel.rdz.grainchaintest.R
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

class GPSTracker(private val mContext: Context) : Service() {
    // flag for GPS status
    var isGPSEnabled = false
    // flag for network status
    var isNetworkEnabled = false
    var location // location
            : Location? = null
    private var latitude = 0.0 // latitude
    private var longitude = 0.0 // longitude
    var bearing = 0f
    // Declaring a Location Manager
    protected var locationManagerGPS: LocationManager? = null
    protected var locationManagerNetWork: LocationManager? = null
    protected var locationManagerPassive: LocationManager? = null
    protected var mGoogleApiClient: GoogleApiClient? = null
    var locationGPS: Location? = null
    var locationNetWork: Location? = null
    var locationPassive: Location? = null
    var locationGoogleApi: Location? = null
    var listenerNetWork: LocationListener? = null
    var listenerGPS: LocationListener? = null
    var listenerPassive: LocationListener? = null
    var mLocationRequest: LocationRequest? = null

    fun getLocation() {
        if (mGoogleApiClient == null) {
            mLocationRequest = LocationRequest.create()
            mLocationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            mLocationRequest?.interval =
                POLLING_FREQ
            mLocationRequest?.fastestInterval =
                FASTEST_UPDATE_FREQ
            mGoogleApiClient = GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(object : ConnectionCallbacks {
                    override fun onConnected(bundle: Bundle?) {
                        LocationServices.getFusedLocationProviderClient(mContext)
                            .requestLocationUpdates(
                                mLocationRequest, object : LocationCallback() {
                                    override fun onLocationResult(locationResult: LocationResult) {
                                        locationGoogleApi = location
                                    }
                                },
                                Looper.myLooper()
                            )
                    }

                    override fun onConnectionSuspended(i: Int) {}
                })
                .addOnConnectionFailedListener { locationGoogleApi = null }
                .addApi(LocationServices.API)
                .build()
            mGoogleApiClient?.connect()
        }
        if (locationManagerGPS == null || locationManagerNetWork == null || locationManagerPassive == null) {
            try {
                if (locationManagerNetWork == null) {
                    locationManagerNetWork = mContext
                        .getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    // getting network status
                    locationManagerNetWork?.isProviderEnabled(LocationManager.NETWORK_PROVIDER)?.let {
                        isNetworkEnabled = it
                    }
                    Log.d(
                        "infoLocationManager",
                        "isNetworkEnabled=$isNetworkEnabled"
                    )
                    //  get location from Network Provider
                    if (isNetworkEnabled) {
                        listenerNetWork = object : LocationListener {
                            override fun onLocationChanged(location: Location) {
                                locationNetWork = location
                                setLocation()
                            }

                            override fun onStatusChanged(
                                provider: String,
                                status: Int,
                                extras: Bundle
                            ) {
                            }

                            override fun onProviderEnabled(provider: String) {}
                            override fun onProviderDisabled(provider: String) {}
                        }
                        if (ActivityCompat.checkSelfPermission(
                                this,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                                this,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            return
                        }
                        locationManagerNetWork?.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(),
                            listenerNetWork
                        )
                    }
                }
                if (locationManagerGPS == null) {
                    locationManagerGPS = mContext
                        .getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    // getting GPS status
                    locationManagerGPS?.isProviderEnabled(LocationManager.GPS_PROVIDER)?.let {
                        isGPSEnabled = it
                    }
                    Log.d("infoLocationManager", "isGPSEnabled=$isGPSEnabled")
                    // if GPS Enabled get lat/long using GPS Services
                    if (isGPSEnabled) {
                        listenerGPS = object : LocationListener {
                            override fun onLocationChanged(location: Location) {
                                locationGPS = location
                                setLocation()
                            }

                            override fun onStatusChanged(
                                provider: String,
                                status: Int,
                                extras: Bundle
                            ) {
                            }

                            override fun onProviderEnabled(provider: String) {}
                            override fun onProviderDisabled(provider: String) {}
                        }
                        locationManagerGPS?.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(),
                            listenerGPS
                        )
                    } else {
                        if (!isMsjGPSShow) {
                            isMsjGPSShow = true
                            showSettingsAlert()
                        }
                    }
                }
                if (locationManagerPassive == null) {
                    locationManagerPassive = mContext
                        .getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    listenerPassive = object : LocationListener {
                        override fun onLocationChanged(location: Location) {
                            locationPassive = location
                            setLocation()
                        }

                        override fun onStatusChanged(
                            provider: String,
                            status: Int,
                            extras: Bundle
                        ) {
                        }

                        override fun onProviderEnabled(provider: String) {}
                        override fun onProviderDisabled(provider: String) {}
                    }
                    locationManagerPassive!!.requestLocationUpdates(
                        LocationManager.PASSIVE_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(),
                        listenerPassive
                    )
                }
                forceUpdateLocation()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            forceUpdateLocation()
        }
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    fun stopUsingGPS() {
        if (locationManagerGPS != null && listenerGPS != null) {
            locationManagerGPS?.removeUpdates(listenerGPS)
            locationManagerGPS = null
        }
        if (locationManagerNetWork != null && listenerNetWork != null) {
            locationManagerNetWork?.removeUpdates(listenerNetWork)
            locationManagerNetWork = null
        }
    }

    /**
     * Function to get latitude
     */
    fun getLatitude(): Double {
        forceUpdateLocation()
        if (location != null) {
            location?.latitude?.let {
                latitude = it
            }
        }
        // return latitude
        return latitude
    }

    /**
     * Function to get longitude
     */
    fun getLongitude(): Double {
        forceUpdateLocation()
        if (location != null) {
            location?.longitude?.let {
                longitude = it
            }
        }
        // return longitude
        return longitude
    }

    /**
     * Function to get speed
     */
    val speed: Double
        get() {
            forceUpdateLocation()
            return if (location != null) {
                if (location!!.hasSpeed()) location!!.speed.toDouble() else 0.0
            } else 0.0
        }

    var isMsjGPSShow = false
    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(mContext)
        // Setting Dialog Title
        alertDialog.setTitle(R.string.app_name)
        // Setting Dialog Message
        alertDialog.setMessage(R.string.gps_setting_msg)
        // On pressing Settings button
        alertDialog.setPositiveButton(R.string.aceptar) { dialog, which ->
            val intent =
                Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            mContext.startActivity(intent)
        }
        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.cancel) { dialog, which -> dialog.cancel() }
        // Showing Alert Message
        alertDialog.show()
    }

    override fun onBind(arg0: Intent): IBinder? {
        return null
    }

    fun setLocation() {
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            locationGoogleApi = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient
            )
            if (locationGoogleApi != null) {
                location = locationGoogleApi
                latitude = location!!.latitude
                longitude = location!!.longitude
                bearing = location!!.bearing
            }
        } else {
            if (locationGPS != null && locationNetWork != null) {
                if (isNetworkAvailable) {
                    location = locationNetWork
                    latitude = location!!.latitude
                    longitude = location!!.longitude
                    bearing = location!!.bearing
                } else {
                    location = locationGPS
                    latitude = location!!.latitude
                    longitude = location!!.longitude
                    bearing = location!!.bearing
                }
                Log.d(
                    "infoLocationManager",
                    "locationNetWork.getAccuracy=" + locationNetWork!!.accuracy
                )
                Log.d(
                    "infoLocationManager",
                    "locationGPS.getAccuracy=" + locationGPS!!.accuracy
                )
            } else {
                if (locationNetWork != null) {
                    location = locationNetWork
                    latitude = location!!.latitude
                    longitude = location!!.longitude
                    bearing = location!!.bearing
                    Log.d(
                        "infoLocationManager",
                        "locationNetWork.getAccuracy=" + locationNetWork!!.accuracy
                    )
                } else if (locationGPS != null) {
                    location = locationGPS
                    latitude = location!!.latitude
                    longitude = location!!.longitude
                    bearing = location!!.bearing
                    Log.d(
                        "infoLocationManager",
                        "locationGPS.getAccuracy=" + locationGPS!!.accuracy
                    )
                }
                if (locationGPS == null && locationNetWork == null) {
                    if (locationPassive != null) {
                        location = locationPassive
                        latitude = location!!.latitude
                        longitude = location!!.longitude
                        bearing = location!!.bearing
                    }
                }
            }
        }
    }

    val currentLocation: Location?
        get() {
            forceUpdateLocation()
            return location
        }

    fun forceUpdateLocation() {
        if (mGoogleApiClient != null) {
            if (!mGoogleApiClient!!.isConnected) {
                mGoogleApiClient!!.connect()
            } else {
                locationGoogleApi =
                    LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
            }
        }
        if (locationManagerNetWork != null) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) { // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            locationNetWork = locationManagerNetWork!!
                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            if (locationNetWork != null) Log.d(
                "infoLocationManager",
                "locationNetWork=" + locationNetWork.toString()
            )
        }
        if (locationManagerGPS != null) {
            locationGPS = locationManagerGPS!!
                .getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (locationGPS != null) Log.d(
                "infoLocationManager",
                "locationGPS=" + locationGPS.toString()
            )
        }
        if (locationManagerPassive != null) {
            locationPassive = locationManagerPassive!!
                .getLastKnownLocation(LocationManager.PASSIVE_PROVIDER)
            if (locationPassive != null) Log.d(
                "infoLocationManager",
                "locationPassive=" + locationPassive.toString()
            )
        }
        setLocation()
    }

    private val isNetworkAvailable: Boolean
        private get() {
            val connectivityManager =
                getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

    companion object {
        // The minimum distance to change Updates in meters
        private const val MIN_DISTANCE_CHANGE_FOR_UPDATES: Long = 5 // 10 meters
        // The minimum time between updates in milliseconds
        private const val MIN_TIME_BW_UPDATES = 1000 * 1 // 1 seconds
            .toLong()
        private const val POLLING_FREQ: Long = 1000
        private const val FASTEST_UPDATE_FREQ: Long = 1000
    }

    init {
        getLocation()
    }
}