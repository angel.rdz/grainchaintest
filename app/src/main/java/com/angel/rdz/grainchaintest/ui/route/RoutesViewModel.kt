package com.angel.rdz.grainchaintest.ui.route

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.angel.rdz.grainchaintest.database.AppDatabase
import com.angel.rdz.grainchaintest.database.entity.Route
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlin.collections.ArrayList

class RoutesViewModel : ViewModel() {

    var routeList: MutableLiveData<ArrayList<Route>> = MutableLiveData()
    var list : ArrayList<Route>? = null
    lateinit var db : AppDatabase

    fun setDataBase(myDb: AppDatabase){
        db = myDb
    }

    fun updateRouteList(){
        GlobalScope.launch {
            list = db.routesDao().getAll() as ArrayList
            routeList.postValue(list)
        }
    }

}