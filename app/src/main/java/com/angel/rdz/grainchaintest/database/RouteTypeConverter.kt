package com.angel.rdz.grainchaintest.database

import androidx.room.TypeConverter
import com.angel.rdz.grainchaintest.database.entity.Route
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class RouteTypeConverter {
    @TypeConverter
    fun stringFromObject(list: List<Route>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun getObjectFromString(jsonString: String?): List<Route> {
        val listType: Type = object : TypeToken<ArrayList<Route>>() {}.type
        return Gson().fromJson(jsonString, listType)
    }
}