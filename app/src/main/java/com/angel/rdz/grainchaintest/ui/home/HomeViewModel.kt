package com.angel.rdz.grainchaintest.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.angel.rdz.grainchaintest.database.AppDatabase
import com.angel.rdz.grainchaintest.database.entity.Route
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {
    var liveDataRouteList: MutableLiveData<ArrayList<LatLng>> = MutableLiveData()
    var list : ArrayList<LatLng> = ArrayList()
    lateinit var db : AppDatabase

    fun setDataBase(myDb: AppDatabase){
        db = myDb
    }

    fun saveRoute(title: String, points: MutableList<LatLng>, duration: String) {
        GlobalScope.launch {
            db.routesDao().insertAll(
                Route(
                    title = title,
                    points = points,
                    time = duration
                )
            )
        }
    }

    fun updateLocation(latitude: Double, longitude: Double){
        val newLocation = LatLng(latitude, longitude)
        list.add(newLocation)
        liveDataRouteList.postValue(list)
    }
}