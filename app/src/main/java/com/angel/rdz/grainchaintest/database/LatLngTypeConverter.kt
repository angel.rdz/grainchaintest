package com.angel.rdz.grainchaintest.database

import androidx.room.TypeConverter
import com.angel.rdz.grainchaintest.database.entity.Route
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class LatLngTypeConverter {

    @TypeConverter
    fun stringFromObject(list: List<LatLng>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun getObjectFromString(jsonString: String?): List<LatLng> {
        val listType: Type = object : TypeToken<ArrayList<LatLng>>() {}.type
        return Gson().fromJson(jsonString, listType)
    }

    @TypeConverter
    fun stringFromRoute(list: List<Route>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun getRouteFromString(jsonString: String?): List<Route> {
        val listType: Type = object : TypeToken<ArrayList<Route>>() {}.type
        return Gson().fromJson(jsonString, listType)
    }


}