package com.angel.rdz.grainchaintest.ui.route

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.angel.rdz.grainchaintest.*
import com.angel.rdz.grainchaintest.database.AppDatabase
import com.angel.rdz.grainchaintest.ui.route.adapter.RouteListAdapter
import com.angel.rdz.grainchaintest.ui.route.interfaces.RoutesInterface
import com.angel.rdz.grainchaintest.database.entity.Route
import com.angel.rdz.grainchaintest.ui.routeDetail.RouteDetailActivity


class RoutesFragment : Fragment(),
    RoutesInterface {

    private lateinit var routesViewModel: RoutesViewModel
    private lateinit var recyclerView: RecyclerView
    private var adapter: RouteListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        routesViewModel = ViewModelProvider(this).get(RoutesViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_routes, container, false)

        AppDatabase.getAppDataBase(requireContext())?.let { mydb ->
            routesViewModel.setDataBase(mydb)
        }

        recyclerView = root.findViewById<RecyclerView>(R.id.my_recycler_view).apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            val dividerItemDecoration = DividerItemDecoration(
                context, (layoutManager as LinearLayoutManager).orientation)
            addItemDecoration(dividerItemDecoration)
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        routesViewModel.routeList.observe(viewLifecycleOwner, Observer {
            if (adapter == null) {
                adapter =
                    RouteListAdapter(
                        it
                    )
                adapter?.listener = this
                recyclerView.adapter = adapter
            } else {
                adapter?.updateList(it)
                recyclerView.adapter = adapter
            }
        })
        routesViewModel.routeList.removeObserver {
            adapter?.notifyDataSetChanged()
        }
    }

    override fun onResume() {
        super.onResume()
        routesViewModel.updateRouteList()
    }

    override fun showDetail(route: Route) {
        val intent = Intent(context, RouteDetailActivity::class.java)
        intent.putExtra("routeId", route.id)
        startActivity(intent)
    }
}
